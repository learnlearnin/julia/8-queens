# 8-queens

This is a solution to the [8 queens chess puzzle](https://en.wikipedia.org/wiki/Eight_queens_puzzle) written in julia language.

## Usage

To simply list all 92 solutions, run `julia queens.jl`

If you want to set specific starting positions, you can pass the cell values as arguments. For example, if you want to set a queen at 2nd row and 3rd column, run `julia queens.jl 2 3`. For even more queens, just keep adding cells, eg `julia queens.jl 2 3 5 6`

## Screencast
[![asciicast](https://asciinema.org/a/sgmIkE9uPru35T8tBIstSQjgh.png)](https://asciinema.org/a/sgmIkE9uPru35T8tBIstSQjgh)
