solutions = 0

function diagonalsfree(board,cell)
    return (topleftfree(board,cell) && bottomleftfree(board,cell) && toprightfree(board,cell) && bottomrightfree(board,cell))
end

function topleftfree(board,cell)
    checkdiagonal(board,cell,-1,-1)
end

function bottomleftfree(board,cell)
    checkdiagonal(board,cell,1,-1)
end

function toprightfree(board,cell)
    checkdiagonal(board,cell,-1,1)
end

function bottomrightfree(board,cell)
    checkdiagonal(board,cell,1,1)
end

function checkdiagonal(board,cell,offsetx,offsety)
    x=cell[1]+offsetx
    y=cell[2]+offsety
    while (0<x && x<9 && 0<y && y<9)
        if board[x,y] == true
            return false
        end
        x=x+offsetx
        y=y+offsety
    end
    return true
end

function rowfree(board,cell)
    for i in 1:8
        if board[cell[1],i] == true
            return false
        end
    end
    return true
end

function columnfree(board,column)
    for i in 1:8
        if board[i,column] == true
            return false
        end
    end
    return true
end

function printboard(board)
    print("===============\n")
    for row in 1:8
        for column in 1:8
            if board[row,column] == true
                print("X ")
            else
                print("- ")
            end
        end
        print("\n")
    end
    print("===============\n")
end

function cellsafe(board,cell)
    return (
        diagonalsfree(board,cell) 
        && rowfree(board,cell) 
        )
end

function findRestQueens(board,column=1)
    if column == 9
        global solutions
        solutions = solutions + 1
        println("Solution ", solutions)
        printboard(board)
        println()
        return true
    end
    if columnfree(board,column)
        for row in 1:8
            if(cellsafe(board,(row,column)))
                guessboard=copy(board)
                guessboard[row,column]=true
                findRestQueens(guessboard,column+1)
            end
        end
        return false
    else
        findRestQueens(board,column+1)
    end
end

board = falses(8,8)

arguments = map(x->parse(Int,x),ARGS)
arglength=length(arguments)
cells = floor(Int, arglength/2)
if arglength>0
    if arglength%2 == 0
        for i in 1:cells
            cell = (arguments[i*2-1],arguments[i*2])
            if(cellsafe(board,cell))
                board[cell[1], cell[2]] = true
                println("Set a starting queen at: ", cell)
            else
                println("Invalid starting position. Can't set queen at ", cell)
                board[cell[1],cell[2]] = true
                printboard(board)
                println()
                quit()
            end
        end
        println("Starting position is:")
        println("=====================")
        printboard(board)
        println()
    else
        print("You need to enter cells that are checked as args. Eg: `julia queens.jl 1 1 3 4` if the top left cell and the cell two rows below and 3 rows to the right of it is taken already")
        quit()
    end
end


findRestQueens(board)

if solutions > 0
    if solutions == 1
        @printf("Found the only solution")
    else
        @printf("Found %d solutions", solutions)
    end
else
    print_with_color(:red, "Unfortunately no solutions exist for this starting combination")
end